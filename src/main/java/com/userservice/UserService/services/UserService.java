package com.userservice.UserService.services;

import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.repositories.UserRepositiory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired
    private UserRepositiory repositiory;

    public List<UserDTO> getOredrByUserId(Long id)
    {
        List<UserDTO> orders = null;
        try
        {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),
                    List.class
            );
        }
        catch (Exception ex)
        {

        }

        return  orders;
    }


    public List<UserDTO> getAllUsersList()
    {
        //LOGGER.info("/==============Enter into getUserService in UserService ======/");
        List<UserDTO> users = null;
        try
        {
            users = repositiory.findAll() // UserEntity
                .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId().toString(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());

        }
        catch (Exception ex)
        {
            LOGGER.warn(ex.getMessage());
        }

        return users;

    }


}

































